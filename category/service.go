package category

type Service interface {
	GetCategory() ([]Category, error)
	GetCategoryByID(input GetCategoryDetailInput) (Category, error)
	CreateCategory(input CreateCategoryInput) (Category, error)
	UpdateCategory(inputID GetCategoryDetailInput,input CreateCategoryInput) (Category, error)
	DeleteCategory(input GetCategoryDetailInput) (Category, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) GetCategory() ([]Category, error) {
	categories, err := s.repository.FindAll()
	if err != nil {
		return categories, err
	}
	return categories, nil
}

func (s *service) GetCategoryByID(input GetCategoryDetailInput) (Category, error) {
	category, err := s.repository.FindByID(input.ID)
	if err != nil {
		return category, err
	}
	return category, nil
}

func (s *service) CreateCategory(input CreateCategoryInput) (Category, error) {
	category := Category{}
	category.Name = input.Name
	newCategory, err := s.repository.Save(category)
	if err != nil {
		return newCategory, err
	}
	return newCategory, nil
}

func (s *service) UpdateCategory(inputID GetCategoryDetailInput, inputData CreateCategoryInput) (Category, error) {
	category, err := s.repository.FindByID(inputID.ID)
	if err != nil {
		return category, err
	}
	category.Name = inputData.Name

	updatedCategory, err := s.repository.Update(category)
	if err != nil {
		return updatedCategory, err
	}
	return updatedCategory, nil
}

func (s *service) DeleteCategory(inputID GetCategoryDetailInput) (Category, error) {
	category, err := s.repository.FindByID(inputID.ID)
	if err != nil {
		return category, err
	}

	deletedCategory, err := s.repository.Delete(category)
	if err != nil {
		return deletedCategory, err
	}
	return deletedCategory, nil
}
