package category

type GetCategoryDetailInput struct {
	ID int `uri:"id" binding:"required"`
}

type CreateCategoryInput struct {
	Name string `json:"name" binding:"required"`
}

