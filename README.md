# test-go-izy

test-go-izy

<!-- Cheatsheet GO Solid Pattern -->

input               -> request
handler             -> controller
services            -> business process
repository          -> query
entitiy             -> define table relation and entity

<!-- How to run -->
- create new database
- import mysql database from /sql
- cp .env.example .env
- change database settings in .env
- run with go run main.go or go run .