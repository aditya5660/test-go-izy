package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/aditya5660/test-go-izy/category"
	"gitlab.com/aditya5660/test-go-izy/helper"
)

// get params di handler
// handler to services
// services to repository
// services menentukan repository yang di call
// repository acccess to db

type categoryHandler struct {
	categoryService category.Service
}

func NewCategoryHandler(categoryService category.Service) *categoryHandler {
	return &categoryHandler{categoryService}
}

// api/v1/categories
func (h *categoryHandler) GetCategories(c *gin.Context) {
	// get data
	categories, err := h.categoryService.GetCategory()
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Error to get categories", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	// format response
	formatter := category.FormatCategories(categories)
	// api response
	response := helper.APIResponse("list of categories", http.StatusOK, "success", formatter)
	// return json
	c.JSON(http.StatusOK, response)
}

// api/v1/categories/:id
func (h *categoryHandler) GetCategory(c *gin.Context) {
	// get input
	var input category.GetCategoryDetailInput
	// err input
	err := c.ShouldBindUri(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Failed to get detail of category", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	// get data
	categories, err := h.categoryService.GetCategoryByID(input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Failed to get detail of category", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	// format response
	formatter := category.FormatCategoryDetail(categories)
	// api response
	response := helper.APIResponse("Get Category successfuly!", http.StatusOK, "success", formatter)
	// return json
	c.JSON(http.StatusOK, response)

}

func (h *categoryHandler) CreateCategory(c *gin.Context) {
	// get input from form data
	var input category.CreateCategoryInput
	// map input from user to struct Register User Input and validate input
	err := c.ShouldBindJSON(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Create Category failed", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	// struct diatas akan di parse setelah service
	newCategory, err := h.categoryService.CreateCategory(input)
	if err != nil {
		response := helper.APIResponse("Create Category failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	// format response
	formatter := category.FormatCategory(newCategory)
	// api response
	response := helper.APIResponse("Create Category Successfully", http.StatusOK, "success", formatter)
	// return json
	c.JSON(http.StatusOK, response)
}

func (h *categoryHandler) UpdateCategory(c *gin.Context) {
	var inputID category.GetCategoryDetailInput

	err := c.ShouldBindUri(&inputID)
	if err != nil {
		response := helper.APIResponse("Failed to update category", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	var inputData category.CreateCategoryInput

	err = c.ShouldBindJSON(&inputData)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}

		response := helper.APIResponse("Failed to update category", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}

	updatedCategory, err := h.categoryService.UpdateCategory(inputID, inputData)
	if err != nil {
		response := helper.APIResponse("Failed to update category", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	response := helper.APIResponse("Success to update category", http.StatusOK, "success", category.FormatCategory(updatedCategory))
	c.JSON(http.StatusOK, response)
}

func (h *categoryHandler) DeleteCategory(c *gin.Context) {
	var inputID category.GetCategoryDetailInput

	err := c.ShouldBindUri(&inputID)
	if err != nil {
		response := helper.APIResponse("Failed to delete category", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	deletedCategory, err := h.categoryService.DeleteCategory(inputID)
	if err != nil {
		response := helper.APIResponse("Failed to delete category", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	response := helper.APIResponse("Success to delete category", http.StatusOK, "success", category.FormatCategory(deletedCategory))
	c.JSON(http.StatusOK, response)
}
