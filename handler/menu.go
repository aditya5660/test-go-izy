package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/aditya5660/test-go-izy/helper"
	"gitlab.com/aditya5660/test-go-izy/menu"
)

// get params di handler
// handler to services
// services to repository
// services menentukan repository yang di call
// repository acccess to db

type menuHandler struct {
	menuService menu.Service
}

func NewMenuHandler(menuService menu.Service) *menuHandler {
	return &menuHandler{menuService}
}

// api/v1/menus
func (h *menuHandler) GetMenus(c *gin.Context) {
	// get data
	menus, err := h.menuService.GetMenu()
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Error to get menus", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	// format response
	formatter := menu.FormatMenus(menus)
	// api response
	response := helper.APIResponse("list of menus", http.StatusOK, "success", formatter)
	// return json
	c.JSON(http.StatusOK, response)
}

// api/v1/menus/:id
func (h *menuHandler) GetMenu(c *gin.Context) {
	// get input
	var input menu.GetMenuDetailInput
	// err input
	err := c.ShouldBindUri(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Failed to get detail of menu", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	// get data
	menus, err := h.menuService.GetMenuByID(input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Failed to get detail of menu", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	// format response
	formatter := menu.FormatDetailMenu(menus)
	// api response
	response := helper.APIResponse("Get Menu successfuly!", http.StatusOK, "success", formatter)
	// return json
	c.JSON(http.StatusOK, response)

}

func (h *menuHandler) CreateMenu(c *gin.Context) {
	// get input from form data
	var input menu.CreateMenuInput
	// map input from user to struct Register User Input and validate input
	err := c.ShouldBindJSON(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Create Menu failed", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	// struct diatas akan di parse setelah service
	newMenu, err := h.menuService.CreateMenu(input)
	if err != nil {
		response := helper.APIResponse("Create Menu failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	// format response
	formatter := menu.FormatMenu(newMenu)
	// api response
	response := helper.APIResponse("Create Menu Successfully", http.StatusOK, "success", formatter)
	// return json
	c.JSON(http.StatusOK, response)
}

func (h *menuHandler) UpdateMenu(c *gin.Context) {
	var inputID menu.GetMenuDetailInput

	err := c.ShouldBindUri(&inputID)
	if err != nil {
		response := helper.APIResponse("Failed to update menu", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	var inputData menu.CreateMenuInput

	err = c.ShouldBindJSON(&inputData)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}

		response := helper.APIResponse("Failed to update menu", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}

	updatedMenu, err := h.menuService.UpdateMenu(inputID, inputData)
	if err != nil {
		response := helper.APIResponse("Failed to update menu", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	response := helper.APIResponse("Success to update menu", http.StatusOK, "success", menu.FormatMenu(updatedMenu))
	c.JSON(http.StatusOK, response)
}

func (h *menuHandler) DeleteMenu(c *gin.Context) {
	var inputID menu.GetMenuDetailInput

	err := c.ShouldBindUri(&inputID)
	if err != nil {
		response := helper.APIResponse("Failed to delete menu", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	deletedMenu, err := h.menuService.DeleteMenu(inputID)
	if err != nil {
		response := helper.APIResponse("Failed to delete menu", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	response := helper.APIResponse("Success to delete menu", http.StatusOK, "success", menu.FormatMenu(deletedMenu))
	c.JSON(http.StatusOK, response)
}
