# ************************************************************
# Sequel Ace SQL dump
# Version 2077
#
# https://sequel-ace.com/
# https://github.com/Sequel-Ace/Sequel-Ace
#
# Host: 127.0.0.1 (MySQL 5.7.23)
# Database: test-izy-db
# Generation Time: 2021-01-23 04:48:16 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'Appetizer','2021-01-23 11:34:07','2021-01-19 14:36:53',NULL),
	(2,'Main Course','2021-01-23 11:34:10','2021-01-19 14:35:55',NULL),
	(3,'Desert','2021-01-23 11:34:13','2021-01-19 14:53:44',NULL);

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table menus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menus`;

CREATE TABLE `menus` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) unsigned NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(255) DEFAULT '',
  `price` varchar(50) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `menus_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;

INSERT INTO `menus` (`id`, `category_id`, `name`, `description`, `price`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,'Fruit Salad','Fruit Salad','45000','2021-01-23 11:46:48','2021-01-20 15:20:57',NULL),
	(2,1,'Vegetable Salad','Vegetable Salad','45000','2021-01-23 11:35:39','2021-01-20 15:20:57',NULL),
	(3,1,'Classic Caesar Salad','Classic Caesar Salad','40000','2021-01-23 11:35:45','2021-01-20 15:22:03',NULL),
	(4,2,'Salmon Steak Tomato Salsa','Salmon Steak Tomato Salsa\n','160000','2021-01-23 11:36:13','2021-01-20 15:22:46',NULL),
	(12,2,'Roasted Half Chicken','Roasted Half Chicken','160000','2021-01-23 11:36:13','2021-01-20 15:22:46',NULL),
	(13,2,'Bebek Goreng Ala Bali','Bebek Goreng Ala Bali','160000','2021-01-23 11:36:13','2021-01-20 15:22:46',NULL),
	(14,2,'Nasi Goreng Ala Ala','Nasi Goreng Ala Ala','160000','2021-01-23 11:36:13','2021-01-20 15:22:46',NULL),
	(15,3,'Ice Cream','Ice Cream','160000','2021-01-23 11:36:13','2021-01-20 15:22:46',NULL);

/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
