package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab.com/aditya5660/test-go-izy/category"
	"gitlab.com/aditya5660/test-go-izy/handler"
	"gitlab.com/aditya5660/test-go-izy/menu"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func init() {
	// Log error if .env file does not exist
	if err := godotenv.Load(); err != nil {
		log.Printf("No .env file found")
	}
}

func main() {
	// connect db
	DbHost := os.Getenv("DB_HOST")
	DbUser := os.Getenv("DB_USER")
	DbPassword := os.Getenv("DB_PASSWORD")
	DbName := os.Getenv("DB_NAME")
	DbPort := os.Getenv("DB_PORT")
	DBURL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", DbUser, DbPassword, DbHost, DbPort, DbName)
	db, err := gorm.Open(mysql.Open(DBURL), &gorm.Config{})
	// return err
	if err != nil {
		log.Fatal(err.Error())
	}

	categoryRepository := category.NewRepository(db)
	categoryService := category.NewService(categoryRepository)
	categoryHandler := handler.NewCategoryHandler(categoryService)

	menuRepository := menu.NewRepository(db)
	menuService := menu.NewService(menuRepository)
	menuHandler := handler.NewMenuHandler(menuService)

	router := gin.Default()
	router.Static("/public/images", "./public/images")
	api := router.Group("/api/v1")

	api.GET("/categories", categoryHandler.GetCategories)
	api.GET("/categories/:id", categoryHandler.GetCategory)
	api.POST("/categories", categoryHandler.CreateCategory)
	api.PUT("/categories/:id", categoryHandler.UpdateCategory)
	api.DELETE("/categories/:id", categoryHandler.DeleteCategory)

	api.GET("/menus", menuHandler.GetMenus)
	api.GET("/menus/:id", menuHandler.GetMenu)
	api.POST("/menus", menuHandler.CreateMenu)
	api.PUT("/menus/:id", menuHandler.UpdateMenu)
	api.DELETE("/menus/:id", menuHandler.DeleteMenu)
	router.Run()
}
