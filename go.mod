module gitlab.com/aditya5660/test-go-izy

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/go-playground/validator/v10 v10.4.1
	github.com/joho/godotenv v1.3.0
	gorm.io/driver/mysql v1.0.3
	gorm.io/gorm v1.20.11
)
