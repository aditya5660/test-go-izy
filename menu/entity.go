package menu

import (
	"time"

	"gitlab.com/aditya5660/test-go-izy/category"
)

type Menu struct {
	ID          int
	CategoryID  int
	Name        string
	Description string
	Price       string
	CreatedAt   time.Time
	UpdatedAt   time.Time
	Category    category.Category
}
