package menu

type Service interface {
	GetMenu() ([]Menu, error)
	GetMenuByID(input GetMenuDetailInput) (Menu, error)
	CreateMenu(input CreateMenuInput) (Menu, error)
	UpdateMenu(inputID GetMenuDetailInput, input CreateMenuInput) (Menu, error)
	DeleteMenu(input GetMenuDetailInput) (Menu, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) GetMenu() ([]Menu, error) {
	menu, err := s.repository.FindAll()
	if err != nil {
		return menu, err
	}
	return menu, nil
}

func (s *service) GetMenuByID(input GetMenuDetailInput) (Menu, error) {
	menu, err := s.repository.FindByID(input.ID)
	if err != nil {
		return menu, err
	}
	return menu, nil
}

func (s *service) CreateMenu(input CreateMenuInput) (Menu, error) {
	menu := Menu{}
	menu.Name = input.Name
	menu.CategoryID = input.CategoryID
	menu.Description = input.Description
	menu.Price = input.Price
	newMenu, err := s.repository.Save(menu)
	if err != nil {
		return newMenu, err
	}
	return newMenu, nil
}

func (s *service) UpdateMenu(inputID GetMenuDetailInput, inputData CreateMenuInput) (Menu, error) {
	menu, err := s.repository.FindByID(inputID.ID)
	if err != nil {
		return menu, err
	}
	menu.Name = inputData.Name
	menu.CategoryID = inputData.CategoryID
	menu.Description = inputData.Description
	menu.Price = inputData.Price

	updatedMenu, err := s.repository.Update(menu)
	if err != nil {
		return updatedMenu, err
	}
	return updatedMenu, nil
}

func (s *service) DeleteMenu(inputID GetMenuDetailInput) (Menu, error) {
	menu, err := s.repository.FindByID(inputID.ID)
	if err != nil {
		return menu, err
	}

	deletedMenu, err := s.repository.Delete(menu)
	if err != nil {
		return deletedMenu, err
	}
	return deletedMenu, nil
}
