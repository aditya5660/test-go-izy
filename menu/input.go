package menu

type GetMenuDetailInput struct {
	ID int `uri:"id" binding:"required"`
}

type CreateMenuInput struct {
	CategoryID  int    `json:"category_id" binding:"required"`
	Name        string `json:"name" binding:"required"`
	Description string `json:"description" binding:"required"`
	Price       string `json:"price" binding:"required"`
}

