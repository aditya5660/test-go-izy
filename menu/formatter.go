package menu

type MenuFormatter struct {
	ID          int                   `json:"id"`
	Name        string                `json:"name"`
	Description string                `json:"description"`
	Price       string                `json:"price"`
	Category    MenuCategoryFormatter `json:"category"`
}

func FormatMenu(menu Menu) MenuFormatter {
	menuFormatter := MenuFormatter{}
	menuFormatter.ID = menu.ID
	menuFormatter.Name = menu.Name
	menuFormatter.Description = menu.Description
	menuFormatter.Price = menu.Price

	category := menu.Category

	menuCategoryFormatter := MenuCategoryFormatter{}
	menuCategoryFormatter.ID = category.ID
	menuCategoryFormatter.Name = category.Name

	menuFormatter.Category = menuCategoryFormatter

	return menuFormatter
}

func FormatMenus(menus []Menu) []MenuFormatter {

	menusFormatter := []MenuFormatter{}
	for _, menu := range menus {
		menuFormatter := FormatMenu(menu)
		menusFormatter = append(menusFormatter, menuFormatter)
	}
	return menusFormatter
}

type MenuDetailFormatter struct {
	ID          int                   `json:"id"`
	Name        string                `json:"name"`
	Description string                `json:"description"`
	Price       string                `json:"price"`
	Category    MenuCategoryFormatter `json:"category"`
}

type MenuCategoryFormatter struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func FormatDetailMenu(menu Menu) MenuDetailFormatter {
	menuDetailFormatter := MenuDetailFormatter{}
	menuDetailFormatter.ID = menu.ID
	menuDetailFormatter.Name = menu.Name
	menuDetailFormatter.Description = menu.Description
	menuDetailFormatter.Price = menu.Price

	category := menu.Category

	menuCategoryFormatter := MenuCategoryFormatter{}
	menuCategoryFormatter.ID = category.ID
	menuCategoryFormatter.Name = category.Name

	menuDetailFormatter.Category = menuCategoryFormatter

	return menuDetailFormatter
}
