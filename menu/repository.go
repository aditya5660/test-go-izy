package menu

import "gorm.io/gorm"

type Repository interface {
	FindAll() ([]Menu, error)
	FindByID(ID int) (Menu, error)
	Save(menu Menu) (Menu, error)
	Update(menu Menu) (Menu, error)
	Delete(menu Menu) (Menu, error)

}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (r *repository) FindAll() ([]Menu, error) {
	var menus []Menu
	err := r.db.Preload("Category").Find(&menus).Error
	if err != nil {
		return menus, err
	}
	return menus, nil
}

func (r *repository) FindByID(ID int) (Menu, error) {
	var menu Menu
	err := r.db.Preload("Category").Where("id = ?", ID).Find(&menu).Error
	if err != nil {
		return menu, err
	}
	return menu, nil
}

func (r *repository) Save(menu Menu) (Menu, error) {
	err := r.db.Create(&menu).Error
	if err != nil {
		return menu, err
	}

	return menu, nil
}
func (r *repository) Update(menu Menu) (Menu, error) {
	err := r.db.Save(&menu).Error

	if err != nil {
		return menu, err
	}

	return menu, nil
}

func (r *repository) Delete(menu Menu) (Menu, error) {
	err := r.db.Delete(&menu).Error

	if err != nil {
		return menu, err
	}

	return menu, nil
}

